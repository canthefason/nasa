import mars.MarsRover;
import mars.exception.InvalidCommandException;
import mars.exception.LostContactException;
import mars.exception.OutOfBoundariesException;
import mars.exception.WrongDirectionException;

/**
 * Created with IntelliJ IDEA.
 * User: canthefason
 * Date: 7/1/12
 * Time: 11:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class main
{
    public static void main(String [] args) {
        MarsRover marsRover = new MarsRover(5,5);

        try {
            marsRover.addRover(1,2,"N");

            marsRover.sendCommand("LMLMLMLMM");

            marsRover.addRover(3, 3, "E");

            marsRover.sendCommand("MMRMMRMRRM");

            marsRover.getFinalPositions();

        } catch (OutOfBoundariesException exc) {
            System.out.println("Rover is out of boundaries");
        } catch (WrongDirectionException exc) {
            System.out.println("Direction is not valid");
        } catch (InvalidCommandException exc) {
            System.out.println("Invalid command exists");
        } catch (LostContactException exc) {
            System.out.println("One of the rovers is out of reach");
        }


    }
}
