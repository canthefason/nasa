/**
 * Created with IntelliJ IDEA.
 * User: canthefason
 * Date: 7/1/12
 * Time: 11:52 PM
 * To change this template use File | Settings | File Templates.
 */
package mars;

import mars.exception.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class MarsRover {
    List<Rover> rovers;

    Coordinate boundaries;

    public MarsRover(int x, int y)
    {
        boundaries = new Coordinate(x,y);
        rovers = new ArrayList<Rover>();
        System.out.println("MarsRover created with Boundaries: "+x +','+y);
    }


    public void addRover(int x, int y, String direction) throws OutOfBoundariesException, WrongDirectionException
    {
        Coordinate coord = new Coordinate(x, y);
        if (boundaries.checkBoundaries(coord)) {
            try {
                Rover rover = new Rover(coord,Direction.valueOf(direction));
                rovers.add(rover);
            } catch (IllegalArgumentException exc) {
                throw new WrongDirectionException();
            }
        } else {
            throw new OutOfBoundariesException();
        }

    }

    public void sendCommand(String command) throws LostContactException, InvalidCommandException
    {
         for (int i=0; i < command.length(); i++) {
             try {
                 Command c = Command.valueOf(Character.toString(command.charAt(i)));
                 Rover rover = rovers.get(rovers.size()-1);
                 BasicRoverMover basicRoverMover = new BasicRoverMover(rover);
                 basicRoverMover.processCommand(c);
                 if (!boundaries.checkBoundaries(rover.getCoord())) {
                     throw new LostContactException();
                 }
             } catch (IllegalArgumentException exc) {
                throw new InvalidCommandException();
             }
         }
    }

    public void getFinalPositions()
    {
        Iterator<Rover> itr = rovers.listIterator();
        while (itr.hasNext()) {
            Rover element = itr.next();
            System.out.println(element.getPosition() + "\n");
        }
    }
}
