package mars;

/**
 * Created with IntelliJ IDEA.
 * User: canthefason
 * Date: 7/1/12
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Coordinate {
    int x;
    int y;

    public Coordinate(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public boolean checkBoundaries(Coordinate coord)
    {
        if (coord.x > this.x || coord.x < 0) {
            return false;
        }

        if (coord.y > this.y || coord.y < 0) {
            return false;
        }

        return true;
    }
}
