/**
 * Created with IntelliJ IDEA.
 * User: canthefason
 * Date: 7/1/12
 * Time: 11:53 PM
 * To change this template use File | Settings | File Templates.
 */
package mars;

public class Rover
{
     private Coordinate coord;

     private Direction head;

     public Rover(Coordinate coord, Direction head)
     {
          this.coord = coord;
          this.head = head;
     }

    public String getPosition()
    {
         return  Integer.toString(coord.x) + ' ' + Integer.toString(coord.y) + ' ' + head;
    }

    public Coordinate getCoord()
    {
        return coord;
    }

    public Direction getHead()
    {
        return head;
    }

    public void setHead(Direction head)
    {
        this.head = head;
    }

}
