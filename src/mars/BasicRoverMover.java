package mars;

/**
 * Created with IntelliJ IDEA.
 * User: canthefason
 * Date: 7/2/12
 * Time: 1:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class BasicRoverMover
{

    private Rover rover;

    public BasicRoverMover(Rover rover)
    {
        this.rover = rover;
    }

    public void processCommand(Command c)
    {
        switch(c) {
            case L:
                this.turnLeft();
                break;
            case R:
                this.turnRight();
                break;
            case M:
                this.move();
                break;
        }
    }

    private void turnLeft()
    {
        switch (this.rover.getHead()) {
            case N:
                this.rover.setHead(Direction.W);
                break;
            case W:
                this.rover.setHead(Direction.S);
                break;
            case S:
                this.rover.setHead(Direction.E);
                break;
            case E:
                this.rover.setHead(Direction.N);
                break;
        }
    }

    private void turnRight()
    {
        switch (this.rover.getHead()) {
            case N:
                this.rover.setHead(Direction.E);
                break;
            case E:
                this.rover.setHead(Direction.S);
                break;
            case S:
                this.rover.setHead(Direction.W);
                break;
            case W:
                this.rover.setHead(Direction.N);
                break;
        }
    }

    private void move()
    {
        switch (this.rover.getHead()) {
            case N:
                this.rover.getCoord().y++;
                break;
            case E:
                this.rover.getCoord().x++;
                break;
            case S:
                this.rover.getCoord().y--;
                break;
            case W:
                this.rover.getCoord().x--;
                break;
        }
    }
}
